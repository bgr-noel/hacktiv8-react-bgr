import React, { Component } from 'react';
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import { Card, Nav, Navbar } from 'react-bootstrap';
import logo from './logo.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin, faFacebookSquare, faTwitterSquare, faGithubSquare } from '@fortawesome/free-brands-svg-icons';
import './App.css';

class App extends Component {
  render() {
      return (
        <div>
          <Navbar sticky="top" bg="light" expand="lg">
            <div className="container">
              <Navbar.Brand href="#">Emmanoel Hastono</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                  <HashRouter>
                    <NavLink className="nav-item nav-link" to="/">Home</NavLink>
                    <NavLink className="nav-item nav-link" to="/profile">Profile</NavLink>
                    <NavLink className="nav-item nav-link" to="/contact">Contact</NavLink>
                  </HashRouter>
                </Nav>
              </Navbar.Collapse>
            </div>
          </Navbar>

          {/* <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResumeMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResumeMenu">
              <div className="navbar-nav">
                <HashRouter>
                  <NavLink className="nav-item nav-link" to="/">Home</NavLink>
                  <NavLink className="nav-item nav-link" to="/profile">Profile</NavLink>
                  <NavLink className="nav-item nav-link" to="/contact">Contact</NavLink>
                </HashRouter>
              </div>
            </div>
          </nav> */}

          {/* <Header textHeader="React First Application" /> */} 
          <section className="container mt-4 mb-4">
            <HashRouter>
              <Card body>
                <Route exact path="/" component={Home} />                    
                <Route path="/profile" component={Profile} />
                <Route path="/contact" component={Contact} />
              </Card>
            </HashRouter>
          </section>
        </div>  
      );
  }
}

class Header extends Component {
  render() {
      return (
          <div>
              <h2>{this.props.textHeader}</h2>
          </div>
      );
  }
}

class Home extends Component {
  render() {
      return (
          <div className="w-100">
              <h1 className="mb-0">Emmanoel Hastono</h1>
              <h5 className="mb-5">+62-822-5735-1669 · e.p.hastono@gmail.com</h5>
              <p className="mb-4">
                Currently pursuing Masters in Information Technology at Universitas Bina Nusantara (BINUS). 
                Received Bachelor in Computer Science from Universitas Gadjah Mada.
                Has experience in PHP, Go, Javascript.
              </p>
              <a href="https://linkedin.com/" target="_blank" rel="noopener">
                <FontAwesomeIcon className="mr-2" icon={ faLinkedin } size="3x" />
              </a>
              <a href="https://facebook.com/" target="_blank" rel="noopener">
                <FontAwesomeIcon className="mr-2" icon={ faFacebookSquare } size="3x" />
              </a>
              <a href="https://twitter.com/" target="_blank" rel="noopener">
                <FontAwesomeIcon className="mr-2" icon={ faTwitterSquare } size="3x" />
              </a>
              <a href="https://github.com/" target="_blank" rel="noopener">
                <FontAwesomeIcon className="mr-2" icon={ faGithubSquare } size="3x" />
              </a>
          </div>
      );
  }
}

class Profile extends Component {
  render() {
      return (
          <div>
              <h2>Profile</h2> 
              <div className="row">
                <div className="col-5 text-right">
                  <b>
                    Birthdate&nbsp;
                  </b>
                </div>
                <div className="col-7">
                  11 November 1994
                </div>
              </div>
          </div>
      );
  }
}

class Contact extends Component {
  render() {
      return (
          <div>
              <h2>Contact</h2> 
              <p>Meow all night having their mate disturbing sleeping humans meow for i'm bored inside, let me out i'm lonely outside.</p>
          </div>
      );
  }
}

export default App;

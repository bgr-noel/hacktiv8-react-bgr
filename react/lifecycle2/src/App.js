import React, { Component } from 'react';

export default class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       elapsed: 0
    };

    this.tick = this.tick.bind(this);
    clearInterval(this.timer);
  }

  componentDidMount() {
    this.timer = setInterval(this.tick, 50);
  }

  tick() {
    this.setState({
      elapsed: new Date() - this.props.start,
    })
  }
  
  render() {
    const elapsed = Math.round(this.state.elapsed / 10) / 10;
    const minute = parseInt(elapsed / 60);
    const second = (elapsed - minute*60).toFixed(1);    

    return (
      <div>
        <p>Contoh ini dimulai sejak <b>{minute}</b> menit dan <b>{second}</b> detik yang lalu</p>
      </div>
    )
  }
}


import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
// import PropTypes from 'prop-types';

// combine state & props
// class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       header: [1, 2, 3],
//       content: "State Content"
//     };
//   }

//   render() {
//     return (
//       <div className="App">
//         <Header textHeader={this.state.header} />
//         <Content textContent={this.state.content} />
//       </div>
//     )
//   }
// }

// props
// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <Header textHeader="Welcome to React" />
//         <Content textContent="Kali ini telah menjadi komponen" />
//       </div>
//     );
//   }
// }

// class Header extends Component {
//   render() {
//     return (
//       <div className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <h2>{this.props.textHeader}</h2>
//       </div>
//     );
//   }
// }

// Header.propTypes = {
//   textHeader: PropTypes.string.isRequired
// }

// class Content extends Component {
//   render() {
//     return (
//       <p className="App-intro">
//         {this.props.textContent}
//       </p>
//     );
//   }
// }

// Content.propTypes = {
//   textContent: PropTypes.string.isRequired
// }

// state
// class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       header: "State Header",
//       content: "State Content"
//     }
//   }

//   changeHeader = event => {
//     this.setState({ header: event.target.value });
//   };

//   render() {
//     return (
//       <div className="App">
//         <header className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <h2>{this.state.header}</h2>
//           <a
//             className="App-link"
//             href="https://reactjs.org"
//             target="_blank"
//             rel="noopener noreferrer"
//           >
//             {this.state.header}
//           </a>

//           <form>            
//             <input
//               type="text"
//               name="username"
//               value={this.state.header}
//               onChange={this.changeHeader}
//             />
//           </form>
//         </header>
//       </div>
//     );
//   }
// }

// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <header className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <p>
//             Edit <code>src/App.js</code> and save to reload.
//           </p>
//           <a
//             className="App-link"
//             href="https://reactjs.org"
//             target="_blank"
//             rel="noopener noreferrer"
//           >
//             Learn React
//           </a>
//         </header>
//       </div>
//     );
//   }
// }

// setState
// class App extends Component {
//   constructor(props) {
//     super(props);

//     this.headers = ["Ini Header dari State", "Header Baru", "Header Baru Lagi", "Header Tambah Lagi"];
//     this.i = 0;

//     this.state = {
//       header: this.headers[this.i],
//       content: "Ini Konten dari State",
//     };

//     this.setStateHandler = this.setStateHandler.bind(this);
//   }

//   setStateHandler() {
//     this.i = (this.i + 1) < this.headers.length ? (this.i + 1) : 0;
//     this.setState({ header: this.headers[this.i] });
//   }

//   render() {
//     return(
//       <div className="App">
//         <div className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <button type="button" onClick={this.setStateHandler}><h2>{this.state.header}</h2></button>
//         </div>
//       </div>
//     );
//   }
// }

// Find DOM Mode

class App extends Component {
  constructor() {
    super();
    this.state = {
      header: "Ini Header dari State"
    };
    this.findDomNodeHandler = this.findDomNodeHandler.bind(this);
  }

  findDomNodeHandler() {
    let myDiv = document.getElementById('myDiv');
    ReactDOM.findDOMNode(myDiv).style.backgroundColor = 'blue';
  }

  render() {
    return (
      <div className="App">
        <div className="App-header" id="myDiv">
          <img onClick={this.findDomNodeHandler} src={logo} className="App-logo" alt="logo" />
          <h2>{this.state.header}</h2>
        </div>
        {/* <h4 className="App-intro">
          <button onClick={this.findDomNodeHandler}>FIND DOM NODE</button>
        </h4> */}
      </div>
    )
  }
}

export default App;

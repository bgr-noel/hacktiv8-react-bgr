import React, { Component } from 'react';

export default class About extends Component {
    render() {
        return (
            <div>
                <h2>About</h2>
                <p>Ham hock ball tip tail, pastrami chicken sausage t-bone filet mignon picanha porchetta.</p>
            </div>
        );
    }
}

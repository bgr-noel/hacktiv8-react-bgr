import React, { Component } from 'react';

export default class Home extends Component {
    render() {
        return (
            <div>
                <h2>Home</h2>
                <p>Spicy jalapeno bacon ipsum dolor amet turkey pork chop ham beef ribs burgdoggen rump andouille tongue pancetta swine brisket pork belly.</p>
            </div>
        );
    }
}

import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from './components/Home';
import About from './components/About';

class App extends Component {
    render() {
        return (
            <div>
                <Header textHeader="React First Application" />
                <HashRouter>
                    <NavLink to="/">Home</NavLink>
                    <NavLink to="/about">About</NavLink>
                    <NavLink to="/contact">Contact</NavLink>
                    <Route exact path="/" component={Home} />                    
                    <Route exact path="/about" component={About} />
                    <Route exact path="/contact" component={Contact} />
                </HashRouter>

                {/* <Router>
                    <div>
                        <nav>
                            <ul>
                                <li>
                                    <NavLink to="/">Home</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/about">About</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/contact">Contact</NavLink>
                                </li>
                            </ul>
                        </nav>

                        <Switch>
                            <Route path="/about">
                                <About />
                            </Route>
                            <Route path="/contact">
                                <Contact />
                            </Route>
                            <Route path="/">
                                <Home />
                            </Route>
                        </Switch>
                    </div>
                </Router> */}
            </div>
        );
    }
}

class Header extends Component {
    render() {
        return (
            <div>
                <h2>{this.props.textHeader}</h2>
            </div>
        );
    }
}

class Contact extends Component {
    render() {
        return (
            <div>
                <h2>Contact</h2> 
                <p>Peer out window, chatter at birds, lure them to mouth pee in human's bed until he cleans the litter box and cuddle no cuddle cuddle love scratch scratch.</p>
            </div>
        );
    }
}

export default App;